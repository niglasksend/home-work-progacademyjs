const array = [1, 2, 3, 4]
const str = 'Hello'

//console.log(array[Symbol.iterator])//выдаст функцию
//console.log(str[Symbol.iterator])//выдаст функцию

const iter = array[Symbol.iterator]()
console.log(iter.next())//выдаёт по очереди символы array

const iter2 = str[Symbol.iterator]()
console.log(iter2.next())//выдаёт по очереди символы str

//или

for (let item of array) {
  console.log(item)//выдаст наполнение массива array
}

for (let item of str) {
  console.log(item)//выдаст наполнение str
}

1:29