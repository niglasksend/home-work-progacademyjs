//Rest - оператор в функции - '...args'

function average(arr) {
  return arr.reduce((acc, i) => acc += i, 0) / arr.length; //метод возвращает среднее значение
}

console.log(average([10,20,30,40]))

//или

function average() {

  return Array.from(arguments).reduce((acc, i) => acc += i, 0) / arguments.length; //метод возвращает среднее значение
}

console.log(average(10,20,30,40))

//или

function average(a, b, ...args) { //также можно задавать переменные которые будут выделены отдельно из массива

  return args.reduce((acc, i) => acc += i, 0) / args.length; //метод возвращает среднее значение
}

console.log(average(10,20,30,40))

//Spread

const array = [1, 2, 3, 5, 8, 13]

console.log(...array) //при вызове покажет наполнение массива без массиваБ развернёт массив
console.log(Math.max(...array))//позволяет найти максимальное значение в массиве
console.log(Math.min(...array))
//или
console.log(Math.max.apply(null, array))

const fib = [1, ...array] //добавляет в текущий массив массив array
console.log(fib) //выведет [1,1,2,3,5,78,13]

//Destructuring

const aray = [1, 2, 3, 5, 8, 13]

//const a = aray[0]
//const b = arey[1]
//или
//const [a, b, ...c] = aray //...c переносить всі залишкові елементи масиву в новий масив
//console.log(a, b)

//или

const [a,, c] = aray
console.log(a, c)

//Object

const address = {
  country: 'Ukraine',
  city: 'Kiev',
  street: 'Banderivska',
  concat: function() {
    return `${this.country} ${this.city} ${this.street}`
  }
}
console.log(address.concat())

const {city, country, street = 'Hreschatic'/**значение будет использовано если streat не будет определена */, concat: addressConcat} = adress
console.log(city) //выдаёт Kiev
console.log(concat.call(adress))// выдаёт Ukraine Kiev Banderivska
//или
console.log(addressConcat.call(adress))// выдаёт Ukraine Kiev Banderivska

const {city, ...rest} = address
console.log(city) //выдаст наполнение переменой city
console.log(rest) //выдаст всё что осталось в adress - Ukraine Banderivska concat: function

//
const newAdress = {...address, street: 'Hreschatic', code: 123} //создаёт копию абъекта address, есть возможность заменять значение переменных Banderivska на Hreschatic  
                                                                //и добавлять новые объекты 'code' например 

console.log(newAdress)