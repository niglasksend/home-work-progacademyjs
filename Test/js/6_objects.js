const cityField = 'city' //переменная для динамического ключа

const job = 'Frontend'

const person = {
  age: 26,
  name: 'Irina',
  job,// возьмёт Frontend из переменной
  [cityField]: 'Kiev', //динамический ключ
  'country-live': 'Ukraine',
  print: () => 'Person', //данный метод не работает с контекстом
  /**toString: function() {
    return Object
    .keys(this)
    .filter(key => key !== 'toString') //вернуть все ключи кроме тех, которые ровняются 'toString'
    .map(key => this[key])
    .join('')*/

    //или
    toString() {
      return Object
      .keys(this)
      .filter(key => key !== 'toString') //вернуть все ключи кроме тех, которые ровняются 'toString'
      .map(key => this[key])
      .join('')

  }
}

//console.log(person.toString())

//Methods
const first = {a: 1};
const second = {b: 2};

console.log(Object.is(20, 20)) //метод сравнения на еквивалентность двух значений 20===20 выдаст true
console.log(Object.assign({}, first, second)) //позволяет объеденять объекты, создаст новый объект
console.log(Object.assign(first, second, {c: 3, d: 4})) //позволяет объеденять объекты, дополнит объект first

//
const obj = Object.assign({}, first, second, {c: 3, d: 4})

console.log(Object.entries(obj)) //выдаст массив из массивов наполненные елементами obj, состоящие из ключа м значения [['a' 1], ['b' 2]] и т.п.
console.log(Object.keys(obj)) //выдаст массив из ключей ['a', 'b'] и т.п.
console.log(Object.values(obj)) //выдаст массив из значений [1, 2] и т.п.

