function sum(a, b){
  return a+b
}
function cube(a){
  return a **3 //возведение в третью степень
}

const sum = (a, b) => a + b; //стрелочный вариант первой функции // лямбда функции

const cube = a => a ** 3  //если аргумент у функции только один его можно писать без () и без {} и return, если в функции только одна строчка кода


console.log(sum(41, 1))
console.log(cube(5))

setTimeout(function(){
  console.log('After one second')
}, 1000) // таймер вывода через 1 секунду
//или
setTimeout(() => console.log('After one second'), 1000)

//Context
function log() {
  console.log(this)
}

const arrowLog = () => console.log(this)

const person = {
  name:"",
  age:'',
  log: log,
  arrowLog: arrowLog,
  delayLog: function() {
    //const self = this
    //global.setTimeout(function() {console.log(self.name + '' + self.age)
      //или
    global.setTimeout(() => { //поскольку стрелочная функция не создаёт своего контекста, то нам не нужна доп переменная
      console.log(this.name + '' + this.age)
    }, 500)
  }
}

person.log()