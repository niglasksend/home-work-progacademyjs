'',"",`` //создание строк

const title = 'Hello';

const isVisible = () => Math.random() > 0.5 // стрелочная функция выдаст true если случайное число будет больше 0,5

const template = `
${isVisible() ? `<p>Visible</p>` : `<p>Invisible</p>`}
<h1 id='demo' style="color:red">Title ${title}</h1>
` //обратные кавычки для отказа от канкотинации "+", все пробелы сохраняются

console.log(template)

//Methods

const str = 'Hello'

console.log(str.starsWith('Hel'))//проверяет начинается ли строка с введённых символов 'Hel' выдаёт true
console.log(str.endsWith('lo'))//проверяет заканчиваетя ли строка с введённых символов 'lo' выдаёт true

console.log(str.includes('el'))//проверяет содержит ли строка с введённых символов 'еl' выдаёт true

console.log(str.repeat(6))//дублирует строчку определённое количество раз (6)


console.log(str.trimEnd())//удаляет пробелы c конца строки trimStart c начала trim - все

console.log(str.padStart(10))//задаёт строке минимальное значение длины, если строка короче в начале добавляется определённое количество пробелов,
console.log(str.padEnd(10))//пробелы добавляются с конца

console.log(str.padStart(10, 1234))//или заполнение заданными символами
