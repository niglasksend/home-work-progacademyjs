if(true){
  var num = 42;
}
console.log(num);

if(true){
  let b = 42; //let виден только в рамках скобок
}
console.log(b);

//Hoisting - нельзя обращаться к переменным до их объявления

function hoisted(){
  age = 26;
}
let age;
hoisted()
console.log(age) //let i const будут работать до объявления переменной

//Const
const COLOR = 'red' //переприсваивать по типу нельзя
console.log(COLOR)

const array = [1,2,3,5,8]
array.push(13) //но можно изменять внутреннее наполнение массива
const obj = {a:42}//также можно проводить внутренние мапуляции  с объектами
console.log(obj)