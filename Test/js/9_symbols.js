const symbol = Symbol('demo')
const other = Symbol('demo')

console.log(symbol === other) //не смотря на то что наполнение символов одинаеовое 'demo' каждый из них всё равно будет уникальным выдаст false

const obj = {
  mame:'Elena',
  demo: 'DEMO',//c символами не конфликтует
  [symbol]: 'meta',
}

console.log(obj.demo)//выдаст DEMO
console.log(obj[symbol])//выдаст мета