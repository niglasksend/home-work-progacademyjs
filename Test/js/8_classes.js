class Person {
  type = "human"
  constructor(name) {
    this.name = name
  }

  greet() {
    console.log(this.name + ' говорит привет!')
  }

}


const max = new Person('Max')
max.greet()
console.log(max)
console.log(max.type)

//prototype

console.log(max.__proto__ === Person.prototype)

class Programer extends Person{ //создаём новый класс который наследует параметры у Person
  constructor(name, job) {
    super(name)

    this._job = job
  }

  get job() {
    return this._job.toUpperCase() //выдаст Frontend большими буквами
  }

  set job(job) {
    if (job.lenth < 2) {
      throw new Error('Validation faild')
    }
    this._job = job
  }

  greet() {
    super.greet()//даёт возможность вызвать greet из объекта основания(' говорит привет!') а также оставляет новый объявленый
    console.log('Rewriten')//при вызове если не будем прописывать greet в новом объекте, то будет использован елемент объекта-основания
  }

}

const frontend = new Programer('Max', 'Frontend')
console.log(frontend)
console.log(frontend.job)

//Static

class Util {
  average (...args) {
    return args.reduce((acc, i) => acc += i, 0) / args.length //среднее значение
  }
}

const util = new Util()
console.log(util.average(1, 1, 2, 3, 5, 8))

//или

class Util {
  static average (...args) {
    return args.reduce((acc, i) => acc += i, 0) / args.length //среднее значение
  }
}

const res = Util.average(1,1,2,3,5,8,13)
console.log(res)