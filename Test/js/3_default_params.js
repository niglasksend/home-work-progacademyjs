const defaultB = 30;

const getDefault = c => c * 2

function compute(a=10, b = getDefault(a)) /**(a = 10, b = getDefault(10)) или (a = 20, b = defaultB) или (a, b = 20 )
                { b=20 -значение по умолчанию, даёт возможность при вызове функции не прописывать данную переменную*/{
  return a+b
}

console.log(compute())

